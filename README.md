# UPT

Unified Provisioning Tool

## Description

UPT is a Python-3 project for provisioning and testing in CKI Pipelines.

Beaker is quite closely tied with restraint. To run restraint, one needs to know
what tests to run, a "fake recipe id" and a hostname. The first two are
no issue. The fake recipe id is used to determine what host (identified by
this recipe id) will run what tests. However, with Beaker, we don't necessarily
know right away what hostname will be provisioned. Because of this, 
UPT waits until the resource is provisioned and only then it can
output restraint commands to run tests.   

Currently, a wrapper / test runner around `restraint` is part of this project.
It uses restraint standalone mode to run tests on a provisioned resource(s)
We realize it may need to be moved away. The design of the modules is made with that in mind.

## Red Hat Internal only components

One of the provisioners (1minutetip) depends on internal RH infrastructure and can't be
used externally.
To use this dependency in UPT, install following package:

`$ dnf install -y qa-tools-workstation`

Please note that the package is updated often and the updates are required for 1minutetip to work.

More info about 1minutetip (only available inside Red Hat): https://wiki.test.redhat.com/BaseOs/Projects/1minuteTIP

## Installation & dependencies

* The project needs `bkr` and `restraint` executables. Install the packages by:
`$ dnf install -y beaker-client restraint-client`

* Optional: ensure your pip and setuptools are up-to-date:

```
pip3 install setuptools --upgrade --user
pip3 install pip --upgrade --user
```

* Install Python libraries & UPT:
`$ cd upt && pip3 install --user --editable .`

* (`upt` and `rcclient` links are created automatically)

## Provisioners status:

* Beaker (legacy workflow):
    * full support: consumes any Beaker XML job to provide provisioning and test running
* AWS:
    * limited AWS setup support through `upt aws-setup`
    * instance choice is hardcoded
* Vagrant:
    * a limited functionality wrapper
    * searches existing vagrant instances using 'vagrant global-status' command to find 'id'
      that matches 'instance_id' under 'misc' key of a host in vagrant provisioner (yaml file).
    * afterwards. the provisioner will run 'vagrant up' on this instance and get ssh-config info
      (ip address/hostname), so we can connect.
* Internal only: 1minutetip:
    * limited functionality
    * handles `<distroRequires />` element to specify host distro

You can consider provisioning a machine without UPT and using just the test runner.
Refer to the YAML input format further below.

## Usage and provisioner format

### Basic usage (legacy workflow)

* Drop `beaker.xml` file to `upt` directory.
* Run command exactly as written here: `upt legacy provision`

This will convert `beaker.xml` to `c_req.yaml` and provision resources according to it.

This request both describes what was provisioned and how to provision a similar
resource again. The provisioning done by `c_req.yaml` will provision the same
resource as if `beaker.xml` was used. The difference is that `c_req.yaml` splits
what tasks are used to provision the resource and what are used to run tests.
`c_req.yaml` also contains hostnames that we received after the provisioning
was finished.

* Afterwards, to run tests on provisioned resources using restraint and report results:
  * `$ rcclient test`

### Basic usage (non-legacy workflow)

* Create `req.yaml` (as in provisioning request)
  * This describes what resource(s) you want to provision and what tests to run.

* To provision resources and wait until ready:
    * `$ upt provision`

* Afterwards, to run tests on provisioned resources using restraint and report results:
    * `$ rcclient test`

### Basic usage explained + provisioner format

Below is an example yaml file content (provisioning request) to provision 1 job/recipe/host
and run 1 task.

```
!<ProvisionData>
provisioners:
  - !<OneMinuteTip>
    rgs:
      - !<ResourceGroup>
        resource_id: J:1234
        ssh_opts: ''
        recipeset: !<RecipeSet>
          hosts:
            - !<Host>
              hostname: host_to_run_on
              recipe_id: 1
              recipe_fill: |-
                <recipe> ... </recipe>
              duration: 2880
          restraint_xml: |-
            <?xml version="1.0" encoding="utf-8"?>
            <job id="1">
             <recipeSet>
              <recipe id="1" job_id="1">
               <task name="/distribution/command">
                <fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.tar.gz#command"/>
                <params>
                 <param name="CKI_NAME" value="test1"/>
                 <param name="CMDS_TO_RUN" value="exit 0"/>
                </params>
              </recipe>
             </recipeSet>
            </job>
        job: ''
        preprovisioned: false
    name: beaker
```

Here are some basic rules:
* A file may use/contain multiple provisioners.
* Each `ResourceGroup` object has 1 `RecipeSet`.
* There can be multiple `ResourceGroup` objects in the file.
* Each `RecipeSet` may have multiple hosts.
* Each `recipe_fill` explains how to provision one (1) host.
* Each `restraint_xml` explains what tests to run for one (1) `RecipeSet`.
* `recipeid` / `hostname` fields explicitly tell what tests to run on what host. See `id` attribute of a recipe in `resraint_xml`.
* Beaker only: `duration` specifies for how long (in seconds) will the host stay provisioned. Each time we start running
tests on the host, we renew this duration, because we certainly don't want the machine to disappear in the middle of testing.

The `!<text>` labels are yaml labels for Python classes,
so yaml library automatically knows how to deserialize into objects.

Running `upt --help` or `rcclient --help` is your friend.
The parameters should be reasonably documented there. If not, then let's
fix that.

### AWS provisioner example

To use AWS provisioner, you need to have an AWS account and your system has to be configured to use it.
This usually includes setting AWS access key id, AWS secret access key and AWS region in config file. 
Refer to https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html for more info.

To be able to provision an instance, a launch template has to exist. The environment variable `AWS_UPT_CONFIG`
is used to specify which template to use.

The example below configures UPT to use a default launch template, which can be created using `upt aws-setup` command.
**NOTE: please use aws-setup command only with a private account, to make sure you don't destroy important settings.**

```
export AWS_UPT_CONFIG='{"launch_template": "upt.default.launch-template", "instance_prefix": "default.staging.i.upt"}'
```

Otherwise, using AWS provisioner is very similar to using Beaker. The biggest difference is that the node is named
`aws`.

When an instance is provisioned, `misc` field and `instance_id` is created in `Host` node.
This allows UPT to use existing running instances using `upt waiton` command, instead of provisioning them. 
This is useful especially during development; you don't have to keep launching and terminating instances.

Notes:
* Currently, the provisioner provisions only `t2.micro` instances (hardcoded).
* `duration` and `resource_id` fields are currently ignored.

```
!<ProvisionData>
provisioners:
  - !<AWS>
    rgs:
      - !<ResourceGroup>
        resource_id: '1'
        ssh_opts: ''
        recipeset: !<RecipeSet>
          hosts:
            - !<Host>
              hostname: hostname
              recipe_id: 1
              recipe_fill:
              duration: 28800
              misc:
                instance_id: i-00000000000000001
          restraint_xml: |-
            <?xml version="1.0" encoding="utf-8"?>
            <job id="1">
             <recipeSet>
              <recipe id="1" >
               <task name="/distribution/command">
                <fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.tar.gz#command"/>
                <params>
                 <param name="CKI_NAME" value="test1"/>
                 <param name="CMDS_TO_RUN" value="exit 0"/>
                </params>
               </task>
             </recipeSet>
            </job>
          id:
        job: <job group retention_tag="60days"><whiteboard>UPT@gitlab:123456 3.18@rhel x86_64</whiteboard></job>
        preprovisioned: false
    name: aws
```

### Notes

Currently, machine has 15 minutes to reboot. If it's longer, it's considered an issue.

### Even more info

See `docs` directory, it contains useful info about some design decisions and explains why UPT works the way it does.
