"""OneMinuteTip provisioner/provider."""
import pathlib
import subprocess

from bs4 import BeautifulSoup as BS
from cki_lib.misc import safe_popen

from plumbing.interface import LimitedSynchronous
from upt.logger import LOGGER


class OneMinuteTip(LimitedSynchronous):
    """OneMinuteTip provisioner."""

    # pylint: disable=no-self-use,no-member,protected-access
    def __init__(self, dict_data=None):
        """Construct instances after deserialization."""
        super().__init__(dict_data=dict_data)

        self.wrapper_path = str(pathlib.Path(pathlib.Path(__file__).absolute().parents[0],
                                             '../plumbing', 'oneminutetip_wrap.py'))

    def get_distro_requires(self, recipe):
        """Extract distro_name value from a distroRequires element."""
        distroreq = recipe.find('distroRequires')
        if not distroreq:
            raise RuntimeError('cannot find distroRequires element')

        distro_name = distroreq.find('distro_name')
        if not distro_name:
            raise RuntimeError('cannot find distro_name element')

        return distro_name['value']

    def provision(self, **kwargs):
        """Provision all hosts in all resource groups."""
        self._provision(**kwargs)

        for resource_group in self.rgs:
            # Write ssh_opts for 1minutetip.
            resource_group.ssh_opts = '-o GSSAPIAuthentication=no -i /usr/share/qa-tools/' \
                                      '1minutetip/1minutetip'

    def provision_host(self, host):
        """Provision a single host."""
        distro_name = self.get_distro_requires(BS(host.recipe_fill, 'xml'))
        stdout, stderr, retcode = safe_popen([self.wrapper_path, 'provision', distro_name],
                                             stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE)
        if stderr:
            LOGGER.error(stderr)

        if retcode:
            raise RuntimeError('failed to provision host')

        host.misc['instance_id'], host.hostname = stdout.strip().split(':')

    def release_resources(self):
        """Release all instances."""
        for resource_group in self.rgs:
            for host in resource_group.hosts():
                if host.misc:
                    safe_popen([self.wrapper_path, 'cancel', host.misc['instance_id']])
