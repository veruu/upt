"""Datastructure definition and validation for provisioning request."""

import time
from typing import List

from ruamel.yaml.scalarstring import PreservedScalarString

from plumbing.serializer import DeserializerBase
from upt.logger import COLORS
from upt.logger import LOGGER
from upt.misc import RET


class Host(DeserializerBase):
    """Datastructure describing a host to provision."""

    duration: int

    # host common, can't use a base class because of serialization
    hostname: str
    recipe_id: int
    recipe_fill: PreservedScalarString

    _done_processing: bool = False
    _panicked: bool = False
    _kernel_under_test_installed: bool = False

    _task_results: list = []
    _planned_tests: list = []
    _rerun_recipe_tasks_from_index: int = None

    misc: dict = {}
    # AWS Only
    _user_data_done: bool = False
    _instance: bool = None


class RecipeSet(DeserializerBase):
    """Datastructure describing a recipeset to provision in Beaker."""

    hosts: List[Host]
    restraint_xml: PreservedScalarString
    # Recipset id like 'RS:1'
    id: str = None

    _attempts_made: int = 0
    _tests_finished: bool = False
    _waiting_for_rerun: bool = False


class ResourceGroup(DeserializerBase):
    """A group of resources provisioned together."""

    resource_id: str
    recipeset: RecipeSet
    job: str
    # Specifies the status of provisioning. If PROVISIONING_PASSED is reached, UPT provisioner has
    # successfully completed, exited and will not touch this file anymore.
    status: str = RET.PROVISIONING_WAITING
    _provisioning_done: bool = False
    _erred_rset_ids: set = set()
    _reprovisioned_rsets_ids: set = set()
    _provisioned_ok_rsets_ids: set = set()
    preprovisioned: bool = False
    # SSH options passed to restraint when making an ssh connection to hostnames in this rg.
    ssh_opts: str = ''

    def hosts(self):
        """Iterate through all hosts."""
        for host in self.recipeset.hosts:
            yield host

    def wait(self, end_conditions, set_reservation_duration, release_rg, **kwargs):
        # pylint: disable=protected-access
        """Wait for resource provisioned by provision() to be ready."""
        print(f'Waiting for {self.resource_id} to be ready...')

        # Re-provisions resources and check their state. Will return a retcode
        # at the end of execution of wait().
        while not end_conditions.evaluate(False, **kwargs):
            # wait a while before checking on the resource state again
            time.sleep(end_conditions.evaluate_wait_time)
            if end_conditions._timeout_eval(end_conditions.start,
                                            end_conditions.provisioning_watchdog_update_time):
                # Once upon provisioning_watchdog_update_time, set overall duration of how long
                # should the system stay up.
                for host in self.hosts():
                    if host.hostname:
                        set_reservation_duration(host)

        time.sleep(end_conditions.evaluate_confirm_time)
        force_recheck = True
        while not end_conditions.evaluate(force_recheck, **kwargs):
            force_recheck = False
            time.sleep(end_conditions.evaluate_wait_time)

        # Provisioning of resource group is about to end.
        self.status = end_conditions.retcode

        if end_conditions.retcode == RET.PROVISIONING_PASSED:
            LOGGER.printc('Resource(s) provisioned.', color=COLORS.GREEN)
        else:
            LOGGER.printc('Failed to provision resource(s) !!! No testing will be done.',
                          color=COLORS.RED)
            # Release resources we've provisioned in this run.
            release_rg(self)

        return end_conditions.retcode
