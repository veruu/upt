#!/usr/bin/env python3
"""OneMinuteTip wrapper."""

import os
import re
import sys

from twisted.internet import protocol
from twisted.internet import reactor

from restraint_wrap.misc import attempt_reactor_stop
from upt.logger import LOGGER

# pylint: disable=no-member,no-self-use


class OneMinuteTipProtocol(protocol.ProcessProtocol):
    # pylint: disable=too-many-instance-attributes
    """OneMinuteTip protocol."""

    rgx_ip = re.compile(r'^(.*?) \| ssh root@([^\n]+).*?')
    instance_prompt = re.compile(
        r'^.*?Select instance which you want to reattach by providing its number.*?$',
        re.IGNORECASE)
    rgx_instance = re.compile(r'^\s*([0-9]+)\s+(.*?) (.*?)$')
    rgx_next = re.compile(r'.*?What to do next.*?', re.IGNORECASE)
    rgx_deleting = re.compile(r'.*?Deleting instance.*?', re.IGNORECASE)

    def __init__(self, cmd_lst, reattach_instance=None, target_state=None):
        """Create an object."""
        # Binary to invoke.
        self.tip_binary = '1minutetip'
        # Arguments passed during shell invocations.
        self.cmd_lst = [self.tip_binary] + cmd_lst
        # Twisted process.
        self.proc = None

        # Instance name to reattach or cancel.
        self.reattach_instance = reattach_instance
        # Current FSM state.
        self.state = None

        # Name of the machine instance.
        self.instance_name = None
        # IPv4 ip of the machine.
        self.ip_address = None

        # A target FSM state we want to reach.
        self.target_state = target_state

        # A table of regexes and methods to execute on match for stdout.
        self.tbl = {
            # Set instance to reattach.
            self.rgx_instance: self.set_instance,
            # Save ip of instance
            self.rgx_ip: self.do_save_ip,
        }

        # A table of regexes and methods to execute on match for stderr.
        self.tbl_stderr = {
            # React to prompt and select instance
            self.instance_prompt: self.do_confirm_instance_prompt,
            self.rgx_next: self.on_rgx_next,
            self.rgx_deleting: self.on_delete_done,
        }

        # Internal flag to signify we're prompted for reattachment number.
        self.selecting_instance = False

    def on_delete_done(self, _):
        """Return that we've reached a state when instance was deleted."""
        return 'DELETE_DONE'

    def on_rgx_next(self, _):
        """Send command to delete an instance and return state."""
        self.write('q')

        return 'WHAT_NEXT'

    def do_save_ip(self, match):
        """Save instance ip address, mark instance as ready and stop reactor.

        Args:
            match: .match(data) returned by a regex
        """
        self.instance_name, self.ip_address = match.groups()

        return 'HAVE_IP'

    def do_confirm_instance_prompt(self, _):
        """Confirm we're asked to select an instance."""
        self.selecting_instance = True
        return 'TO_SELECT_INSTANCE'

    def set_instance(self, match):
        """Answer prompt selecting instance to reattach.

        Args:
            match: .match(data) returned by a regex
        """
        if self.selecting_instance and match.group(2) == self.reattach_instance:
            self.write(str(match.group(1)))
            self.selecting_instance = False

            return 'SET_INSTANCE'
        return self.state

    def decide_using_table(self, data, table):
        """Match input data against a table of regexes.

        Args:
            data: str, like 'line output'
            table: dict, {rgx2match: func2run, ...}
        """
        for rgx, value in table.items():
            match = rgx.match(data)
            if match:
                self.state = value(match)
                if self.state == self.target_state:
                    attempt_reactor_stop(reactor)
                    return

                return

    def outReceived(self, data):
        """Process incoming stdout data from 1minutetip."""
        data = data.decode('utf-8').strip()
        if data:
            for line in data.splitlines():
                self.decide_using_table(line, self.tbl)

    def errReceived(self, data):
        """Process incoming stderr data from 1minutetip."""
        data = data.decode('utf-8').strip()
        if data:
            if 'ERROR' in data:
                print(data)

            self.decide_using_table(data, self.tbl_stderr)

    def processEnded(self, reason):
        # pylint: disable=no-member
        """Cleanup on process exit."""
        LOGGER.info('* 1minutetip protocol %s retcode: %s reason: %s', 'was SIGPIPEd!' if
                    'ended by signal 13' in str(reason.value) else 'ended.', reason.value.exitCode,
                    reason.value)
        attempt_reactor_stop(reactor)

    def write(self, data):
        """Write data to process stdin.

        Args:
            data: str, data to write to stdin
        """
        self.transport.write((data + '\n').encode('utf-8'))

    @classmethod
    def reattach(cls, instance_name):
        """Reattach user instance with number instance_no.

        Args:
            instance_no: int, number (e.g. 1) that identifies the instance
        """
        return OneMinuteTipProtocol(['-n', '-r'], reattach_instance=instance_name,
                                    target_state='HAVE_IP').spawn()

    def spawn(self):
        """Spawn a process."""
        self.proc = reactor.spawnProcess(self, self.tip_binary, self.cmd_lst, env=os.environ)
        return self

    @classmethod
    def provision(cls, args):
        """Start/provision 1minutetip machine and return protocol object for accessing it."""
        return OneMinuteTipProtocol(['--restraint', '-n'] + args, target_state='HAVE_IP').spawn()

    @classmethod
    def cancel_instance(cls, instance_name):
        """Cancel/delete an instance, freeing resources."""
        return OneMinuteTipProtocol(['-r'], reattach_instance=instance_name,
                                    target_state='DELETE_DONE').spawn()


def main():
    """Do all."""
    arg = sys.argv[1]
    if arg == 'cancel':
        proto = OneMinuteTipProtocol.cancel_instance(sys.argv[2])
    elif arg == 'provision':
        proto = OneMinuteTipProtocol.provision([sys.argv[2]])
    elif arg == 'reattach':
        proto = OneMinuteTipProtocol.reattach(sys.argv[2])
    else:
        raise RuntimeError('invalid cmd-line arguments')

    reactor.run()
    print(f'{proto.instance_name}:{proto.ip_address}')


if __name__ == '__main__':
    main()
