"""Test cases for 1minutetip helper module."""
import os
import sys
import unittest
from unittest import mock

from plumbing.oneminutetip_wrap import OneMinuteTipProtocol
from plumbing.oneminutetip_wrap import main


class TestOneMinuteTipWrap(unittest.TestCase):
    """Test cases for 1minutetip helper module."""

    def setUp(self) -> None:
        self.tip_wrap = OneMinuteTipProtocol(['a1', 'a2'])

    def test_on_delete_done(self):
        """Ensure on_delete_done works."""
        self.assertEqual('DELETE_DONE', self.tip_wrap.on_delete_done(None))

    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.write', mock.Mock())
    def test_on_rgx_next(self):
        """Ensure on_rgx_next works."""
        self.assertEqual('WHAT_NEXT', self.tip_wrap.on_rgx_next(None))

    def test_do_save_ip(self):
        """Ensure do_save_ip works."""
        ret_mock = mock.Mock()
        ret_mock.groups.return_value = ('inst', '10.0.0.1')
        self.assertEqual('HAVE_IP', self.tip_wrap.do_save_ip(ret_mock))

    def test_do_confirm_instance_prompt(self):
        """Ensure do_confirm_instance_prompt works."""
        with mock.patch.object(self.tip_wrap, 'selecting_instance', False):
            self.assertEqual('TO_SELECT_INSTANCE', self.tip_wrap.do_confirm_instance_prompt(None))
            self.assertTrue(self.tip_wrap.selecting_instance)

    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.write', mock.Mock())
    def test_set_instance(self):
        """Ensure set_instance works."""
        mock_match = mock.Mock()
        mock_match.group.return_value = 'inst-2'
        with mock.patch.object(self.tip_wrap, 'selecting_instance', True):
            with mock.patch.object(self.tip_wrap, 'reattach_instance', 'inst-2'):
                self.assertEqual('SET_INSTANCE', self.tip_wrap.set_instance(mock_match))

    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.write', mock.Mock())
    def test_set_instance_no_match(self):
        """Ensure set_instance does not break when there is no match."""
        mock_match = mock.Mock()
        mock_match.group.return_value = ''
        with mock.patch.object(self.tip_wrap, 'selecting_instance', True):
            with mock.patch.object(self.tip_wrap, 'reattach_instance', 'inst-2'):
                self.assertEqual(self.tip_wrap.state, self.tip_wrap.set_instance(mock_match))

    @mock.patch('plumbing.oneminutetip_wrap.attempt_reactor_stop')
    def test_decide_using_table(self, mock_attempt):
        """Ensure decide_using_table works."""
        self.tip_wrap.decide_using_table('invalid', self.tip_wrap.tbl)
        mock_attempt.assert_not_called()

        with mock.patch.object(self.tip_wrap, 'target_state', 'invalid-state'):
            self.tip_wrap.decide_using_table('Deleting instance blah', self.tip_wrap.tbl_stderr)
            mock_attempt.assert_not_called()

        with mock.patch.object(self.tip_wrap, 'target_state', 'DELETE_DONE'):
            self.tip_wrap.decide_using_table('Deleting instance blah', self.tip_wrap.tbl_stderr)
            mock_attempt.assert_called()

    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.decide_using_table')
    def test_outReceived(self, mock_decide):
        # pylint: disable=invalid-name
        """Ensure outReceived works."""
        self.tip_wrap.outReceived(b'blah')
        mock_decide.assert_called_with('blah', self.tip_wrap.tbl)

    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.decide_using_table')
    def test_outReceived_no_data(self, mock_decide):
        """Ensure outReceived does what it should with no data."""
        self.tip_wrap.outReceived(b'')
        mock_decide.assert_not_called()

    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.decide_using_table')
    def test_errReceived_no_data(self, mock_decide):
        """Ensure errReceived does what it should with no data."""
        self.tip_wrap.errReceived(b'')
        mock_decide.assert_not_called()

    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.decide_using_table')
    @mock.patch('builtins.print')
    def test_errReceived(self, mock_print, mock_decide):
        # pylint: disable=invalid-name
        """Ensure errReceived works."""
        self.tip_wrap.errReceived(b'blah')
        mock_decide.assert_any_call('blah', self.tip_wrap.tbl_stderr)

        self.tip_wrap.errReceived(b'ERROR')
        mock_decide.assert_any_call('ERROR', self.tip_wrap.tbl_stderr)
        mock_print.assert_called_with('ERROR')

    @mock.patch('plumbing.oneminutetip_wrap.attempt_reactor_stop')
    @mock.patch('plumbing.oneminutetip_wrap.LOGGER.info')
    def test_processEnded(self, mock_info, mock_attempt):
        # pylint: disable=invalid-name
        """Ensure processEnded works."""
        mock_reason = mock.Mock()

        self.tip_wrap.processEnded(mock_reason)
        mock_attempt.assert_called()
        mock_info.assert_called_with('* 1minutetip protocol %s retcode: %s reason: %s',
                                     'was SIGPIPEd!' if
                                     'ended by signal 13' in str(mock_reason.value) else 'ended.',
                                     mock_reason.value.exitCode, mock_reason.value)

    def test_write(self):
        """Ensure write works."""
        with mock.patch.object(self.tip_wrap, 'transport') as mock_transport:
            data = 'blah'
            self.tip_wrap.write(data)
            mock_transport.write.assert_called_with((data + '\n').encode('utf-8'))

    @mock.patch('plumbing.oneminutetip_wrap.reactor.spawnProcess')
    def test_reattach(self, mock_spawn_process):
        """Ensure reattach works."""
        result = OneMinuteTipProtocol.reattach('blah')
        mock_spawn_process.assert_called_with(result, result.tip_binary, result.cmd_lst,
                                              env=os.environ)
        self.assertEqual(['1minutetip', '-n', '-r'], result.cmd_lst)
        self.assertEqual('blah', result.reattach_instance)
        self.assertEqual('HAVE_IP', result.target_state)

    @mock.patch('plumbing.oneminutetip_wrap.reactor.spawnProcess')
    def test_provision(self, mock_spawn_process):
        """Ensure provision works."""
        result = OneMinuteTipProtocol.provision(['blah'])
        mock_spawn_process.assert_called_with(result, result.tip_binary, result.cmd_lst,
                                              env=os.environ)
        self.assertEqual(['1minutetip', '--restraint', '-n', 'blah'], result.cmd_lst)
        self.assertEqual('HAVE_IP', result.target_state)

    @mock.patch('plumbing.oneminutetip_wrap.reactor.spawnProcess')
    def test_cancel_instance(self, mock_spawn_process):
        """Ensure cancel_instance works."""
        result = OneMinuteTipProtocol.cancel_instance('blah')
        mock_spawn_process.assert_called_with(result, result.tip_binary, result.cmd_lst,
                                              env=os.environ)
        self.assertEqual(['1minutetip', '-r'], result.cmd_lst)
        self.assertEqual('blah', result.reattach_instance)
        self.assertEqual('DELETE_DONE', result.target_state)

    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.cancel_instance')
    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.provision')
    @mock.patch('plumbing.oneminutetip_wrap.OneMinuteTipProtocol.reattach')
    @mock.patch('plumbing.oneminutetip_wrap.reactor.run')
    @mock.patch('builtins.print')
    def test_main(self, mock_print, mock_run, mock_reattach, mock_provision, mock_cancel):
        # pylint: disable=too-many-arguments
        """Ensure main works."""
        with mock.patch.object(sys, 'argv', ['main', 'cancel', 'instance']):
            main()
            mock_cancel.assert_called()

        with mock.patch.object(sys, 'argv', ['main', 'provision', 'instance']):
            main()
            mock_provision.assert_called()

        with mock.patch.object(sys, 'argv', ['main', 'reattach', 'instance']):
            main()
            mock_reattach.assert_called()

        with mock.patch.object(sys, 'argv', ['main', 'invalid-command', 'instance']):
            with self.assertRaises(RuntimeError):
                main()

        assert mock_run.call_count == 3
        assert mock_print.call_count == 3
