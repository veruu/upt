"""Test cases for legacy module."""
import pathlib
import unittest
from unittest import mock

from bs4 import BeautifulSoup as BS
from cki_lib.misc import tempfile_from_string
from click.testing import CliRunner

from plumbing.objects import RecipeSet
from tests.const import ASSETS_DIR
from upt.__main__ import cli
from upt.legacy import adjust_job_element
from upt.legacy import convert_xml


class TestLegacy(unittest.TestCase):
    """Test cases for legacy module."""

    def setUp(self) -> None:
        self.xmlpath = pathlib.Path(ASSETS_DIR, 'beaker_xml')
        self.xml = self.xmlpath.read_text()

        self.mock_logger_add_fhandler = mock.patch('upt.__main__.logger_add_fhandler', mock.Mock())
        self.mock_logger_add_fhandler2 = mock.patch('upt.legacy.logger_add_fhandler', mock.Mock())

        self.mock_logger_add_fhandler.start()
        self.mock_logger_add_fhandler2.start()

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_convert_xml_no_exclude(self):
        """Ensure convert_xml works without excluding tests."""
        # Don't exclude any tasks from xml, override whiteboard to contain 'UPT' to distinguish jobs
        kwargs = {'excluded_hosts': '', 'force_host_duration': False}
        # Do the conversion`
        provision_data = convert_xml(self.xml, 1, **kwargs)
        bkr = provision_data.get_provisioner('beaker')

        # Exclude file not used, task_with_fetch should be present
        self.assertIn('task id="1" name="task_with_fetch"', bkr.rgs[0].recipeset.restraint_xml)
        # task_without_fetch has no fetch element, so it is removed
        self.assertNotIn('task name="task_without_fetch"', bkr.rgs[0].recipeset.restraint_xml)

    @mock.patch('upt.logger.LOGGER.warning')
    @mock.patch('builtins.print', mock.Mock())
    def test_convert_xml(self, mock_err):
        # pylint: disable=too-many-locals
        """Ensure convert_xml works with excluding tests."""
        with tempfile_from_string(b'hostname1') as excluded_hosts:
            with tempfile_from_string(b'task_with_fetch\n') as exclude_file:
                # Exclude 1 task from xml, override whiteboard to contain 'UPT' to distinguish jobs
                kwargs = {'exclude': exclude_file, 'force_host_duration': False,
                          'excluded_hosts': excluded_hosts}
                # Do the conversion
                provision_data = convert_xml(self.xml, None, **kwargs)

        bkr = provision_data.get_provisioner('beaker')

        # Check objects count. Two recipeSets -> two resource_groups, 1 recipeSet each.
        self.assertEqual(len(bkr.rgs), 2)
        rg0_recipeset = bkr.rgs[0].recipeset
        rg1_recipeset = bkr.rgs[1].recipeset
        self.assertIsInstance(rg0_recipeset, RecipeSet)
        self.assertIsInstance(rg1_recipeset, RecipeSet)
        # First rg: 2 hosts, second rg: 4 hosts, but 1 is deleted (provisioning issue).
        rg0_hosts = rg0_recipeset.hosts
        rg1_hosts = rg1_recipeset.hosts
        self.assertEqual(len(rg0_hosts), 2)
        self.assertEqual(len(rg1_hosts), 3)

        # Test recipe_fill split.
        rg0_host0, rg0_host1 = rg0_hosts
        rg1_host0, rg1_host1, rg1_host2 = rg1_hosts

        # Task name from exclude file was deleted
        self.assertNotIn('task name="task_with_fetch"', rg0_recipeset.restraint_xml)

        self.assertIn('RHEL-4', rg0_host0.recipe_fill)
        self.assertIn('RHEL-5', rg0_host1.recipe_fill)
        self.assertIn('RHEL-6', rg1_host0.recipe_fill)
        self.assertIn('RHEL-7', rg1_host1.recipe_fill)
        self.assertIn('RHEL-8', rg1_host2.recipe_fill)

        for host in rg0_hosts + rg1_hosts:
            self.assertIn('<hostname op="!=" value="hostname1"/>', host.recipe_fill)

        mock_err.assert_called()

    @mock.patch('upt.logger.LOGGER.warning')
    def test_convert_api(self, mock_warning):
        """Test that click api (convert) works."""

        runner = CliRunner()

        with tempfile_from_string(b'') as rcfile:
            with tempfile_from_string(b'') as ignore:
                response = runner.invoke(cli, ['--rc', rcfile, 'legacy', 'convert', '-i',
                                               self.xmlpath, '-r', ignore])
                assert response.exit_code == 0

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    @mock.patch('upt.legacy.convert_xml')
    @mock.patch('upt.legacy.ProvisionerGlue')
    def test_provision_api(self, mock_glue, mock_convert):
        """Test that click api (provision) works."""
        mock_serialize = mock.Mock()
        mock_convert.return_value = mock_serialize
        mock_glue.return_value.run_provisioners.return_value = 0

        runner = CliRunner()
        with runner.isolated_filesystem():
            pathlib.Path('whatever-rc').touch()
            pathlib.Path('whatever-ignore').touch()
            response = runner.invoke(cli, ['--rc', 'whatever-rc', 'legacy', 'provision', '-i',
                                           self.xmlpath, '-r', 'whatever-ignore'])
            self.assertIn('Provisioning resources (legacy mode)...', response.stdout)
            self.assertEqual(0, response.exit_code)
            mock_serialize.serialize2file.assert_called()

    def test_adjust_job_element(self):
        """Ensure adjust_job_element works."""
        # Test removing group.
        job = BS('<job group="cki" />', 'xml').find('job')
        adjust_job_element(job, '')
        self.assertIsNone(job.get('group'))

        # Test preserving group.
        job = BS('<job group="cki" />', 'xml').find('job')
        adjust_job_element(job, None)
        self.assertEqual('cki', job.get('group'))

        # Test setting group.
        job = BS('<job group="cki" />', 'xml').find('job')
        adjust_job_element(job, 'whale')
        self.assertEqual('whale', job.get('group'))
