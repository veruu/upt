"""Test cases for upt __main__ module."""
import pathlib
import unittest
from unittest import mock

from click.testing import CliRunner

from upt.__main__ import cli
from upt.__main__ import main
from upt.misc import RET


class TestUPT(unittest.TestCase):
    """Test cases for upt __main__ module."""

    def setUp(self):
        self.mock_logger_add_fhandler = mock.patch('upt.__main__.logger_add_fhandler', mock.Mock())
        self.mock_logger_add_fhandler.start()

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    @mock.patch('upt.__main__.ProvisionerGlue.run_provisioners')
    def test_provision_api_main(self, mock_run):
        """Test that main click api (provision) works."""
        runner = CliRunner()
        mock_run.return_value = RET.PROVISIONING_PASSED

        with runner.isolated_filesystem():
            pathlib.Path('rcfile').touch()
            pathlib.Path('ignore').touch()
            response = runner.invoke(cli, ['--rc', 'rcfile', 'provision', '-r', 'ignore'])
            self.assertEqual(response.exit_code, 0)
            mock_run.assert_called()

    @mock.patch('upt.__main__.ProvisionData.deserialize_file')
    def test_cancel_api(self, mock_deserialize_file):
        """Test that click api (cancel) works."""
        runner = CliRunner()
        mock_prov = mock.Mock()
        mock_deserialize_file.return_value.provisioners = [mock_prov]

        with runner.isolated_filesystem():
            pathlib.Path('rcfile').touch()
            pathlib.Path('ignore').touch()
            response = runner.invoke(cli, ['--rc', 'rcfile', 'cancel', '-r', 'ignore'])
            mock_prov.release_resources.assert_called()
            self.assertEqual(response.exit_code, 0)

    @mock.patch('upt.__main__.truncate_logs', mock.Mock())
    @mock.patch('upt.__main__.cli', mock.Mock())
    def test_main_method(self):
        # pylint: disable=no-self-use
        """Ensure main works."""

        main()

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('plumbing.aws_setup.AWSSetup.setup', new=lambda *args: None)
    def test_aws_setup_api(self):
        # pylint: disable=no-self-use
        """Test that click api (aws-setup) works."""
        runner = CliRunner()

        with runner.isolated_filesystem():
            pathlib.Path('rcfile').touch()
            runner.invoke(cli, ['--rc', 'rcfile', 'aws-setup', '-k', 'rcfile'])

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('plumbing.aws_setup.AWSSetup.setup', new=lambda *args: None)
    def test_aws_setup_api_with_ip(self):
        """Test that click api (aws-setup) works."""
        runner = CliRunner()
        with runner.isolated_filesystem():
            pathlib.Path('rcfile').touch()
            response = runner.invoke(cli, ['--rc', 'rcfile', 'aws-setup', '-k', 'rcfile', '--ip',
                                           '0.0.0.0'])
            self.assertEqual(response.exit_code, 0)

    @mock.patch('builtins.print')
    @mock.patch('upt.__main__.ProvisionerGlue.do_wait')
    def test_waiton_api(self, mock_do_wait, mock_print):
        """Test that click api (waiton) works."""
        runner = CliRunner()
        mock_do_wait.return_value = RET.PROVISIONING_PASSED

        with runner.isolated_filesystem():
            pathlib.Path('rcfile').touch()
            pathlib.Path('ignore').touch()
            response = runner.invoke(cli, ['--rc', 'rcfile', 'waiton', '-r', 'ignore'])
            self.assertEqual(response.exit_code, 0)

            mock_print.assert_any_call('UPT CLI started')
            mock_print.assert_any_call('Waiting on existing provisioning request...')
