"""Generate restraint shell commands from provisioning request."""
import os
import tempfile

from bs4 import BeautifulSoup as BS

from upt import const
from upt.logger import LOGGER
from upt.misc import compute_tasks_duration


class ShellWrap:
    # pylint: disable=too-many-instance-attributes
    """Generate restraint shell commands from provisioning request."""

    def __init__(self, resource_group, **kwargs):
        """Create the object."""
        # Is strict keycheck used for ssh?
        self.keycheck = kwargs['keycheck']
        # Number of restraint connection retries
        self.conn_retries = 15

        # pylint: disable=consider-using-with
        # Directory to temporarily store output
        self.tempdir = tempfile.TemporaryDirectory()
        # pylint: enable=consider-using-with
        fpath = self.tempdir.name

        # Restraint job xml path
        self.job_abspath = os.path.abspath(f'{fpath}/{const.RSTRNT_JOB_XML}')

        # Commands to invoke restraint
        self.restraint_commands = ''

        # Resource group for this instance
        self.resource_group = resource_group

        # parsed XML that is being run
        self.run_soup = None

        # Prepare commands and write job.xml
        self.write_rundata()

        self.cleanup_done = False

    def __del__(self):
        """Clean temp output directory."""
        self.tempdir.cleanup()

    @staticmethod
    def update_duration(host, tasks):
        """Update how long should the host stay up."""
        # Add-up duration. Use 120%.
        host.duration = compute_tasks_duration(tasks)
        host.duration = int(host.duration / 100 * 120) if host.duration else const.DEFAULT_RESERVESYS_DURATION
        # Save this info in debug log
        LOGGER.debug('* %i has %0.2fh length', host.recipe_id, host.duration / 3600)

    def create_job_xml(self):
        # pylint: disable=protected-access
        """Create restraint job.xml from recipeset xmls."""
        hosts = []

        soup = BS('<job />', 'xml')
        self.run_soup = soup.find('job')

        # Craft restraint command. Example for 1 recipe: $ restraint --host 1=root@1.2.3.4 --job restraint.xml

        rs_hosts = list(filter(lambda host: host.recipe_id is not None, self.resource_group.recipeset.hosts))
        for host in rs_hosts:
            hosts.append(f'--host {host.recipe_id}=root@{host.hostname}')

        for rec in BS(self.resource_group.recipeset.restraint_xml, 'xml').find_all('recipeSet'):
            self.run_soup.append(rec)

        # Create XML without tasks that were run OK.
        for recipe, host in zip(self.run_soup.findAll('recipe'), rs_hosts):
            if host._rerun_recipe_tasks_from_index is not None:
                to_decompose = []
                for i, task in enumerate(recipe.findAll('task')):
                    if i + 1 < host._rerun_recipe_tasks_from_index:
                        to_decompose.append(task)
                for task in to_decompose:
                    task.decompose()

            # Update how long will the host run
            self.update_duration(host, recipe.findAll('task'))

        return soup.prettify(), hosts

    def write_rundata(self):
        """Write restraint runner commands and ssh aliases."""
        # Create final restraint job xml
        xml, hosts = self.create_job_xml()

        # Write restraint job.xml
        with open(self.job_abspath, 'w') as fhandle:
            fhandle.write(xml)

        opts = f'-o PubkeyAcceptedKeyTypes=+ssh-rsa ' \
               f'-o ServerAliveInterval=60 -o ServerAliveCountMax=5 -o StrictHostKeyChecking={self.keycheck}' + \
               (' ' + self.resource_group.ssh_opts if self.resource_group.ssh_opts else '')
        # Prepare restraint command
        cmd = f'restraint --conn-retries {self.conn_retries} ' \
              f'--job {self.job_abspath} --rsh "ssh {opts}"'  f' {" ".join(hosts)} '

        # Prepare restraint command.
        self.restraint_commands = cmd
