"""A result of a task run, with extra arguments to simplify working with it."""
from datetime import datetime
import os
import re

from bs4 import BeautifulSoup as BS

from restraint_wrap.const import RGX_MAINTAINERS
from upt.misc import is_task_waived


class TaskResult:
    # pylint: disable=too-many-instance-attributes,too-many-arguments,too-few-public-methods,too-many-locals
    """A result of task that was run."""

    def __init__(self, host, task, task_id, result, status, start_time=None, prev_task=None, lwd_hit=False,
                 ewd_hit=False):
        """Create the object."""
        # The task result is identified by recipe_id
        self.recipe_id = int(host.recipe_id)
        self.task = task
        self.task_id = int(task_id)
        self.prefix_recipe_id_path = f'recipes/{self.recipe_id}'
        self.start_time = self.get_start_time(self.recipe_id, self.task_id, host._task_results) if start_time is None \
            else start_time
        # Result & status fields from restraint.
        self.result = result
        self.status = status
        # Aggregated result, called "status" in kcidb. Set by protocol runner on each action.
        self.kcidb_status = None
        self.prev_task = prev_task
        self.host = host
        self.statusresult = status if not result else f'{status}: {result}'

        # Derived
        self.waived = is_task_waived(task)
        self.testname = task['name']
        self.prv_tsk_panicked_waived = prev_task.waived and prev_task.result == 'Panic' if prev_task else False

        soup = BS(str(task), 'xml')
        fetch = soup.find('fetch')
        self.fetch_url = fetch.get('url') if fetch else None

        self.test_maintainers = []
        # Comes from kpet-db.
        for param in soup.find_all('param', attrs={'name': 'CKI_MAINTAINERS'}):
            for name, email, gitlab_name in re.findall(RGX_MAINTAINERS, str(param['value'])):
                self.test_maintainers.append({'name': name, 'email': email, 'gitlab': gitlab_name})

        # comes from kpet-db
        param = soup.find('param', attrs={'name': 'CKI_UNIVERSAL_ID'})
        self.universal_id = param.get('value', '') if param else None

        param = soup.find('param', attrs={'name': 'CKI_NAME'})
        self.cki_name = param.get('value', '') if param else None

        self.subtask_results = []
        self.lwd_hit = lwd_hit
        self.ewd_hit = ewd_hit

    @staticmethod
    def get_most_recent_result(recipe_id, task_id, task_results):
        """Get the most recent task state transition."""
        for task_result in reversed(task_results):
            if task_result.task_id == task_id and task_result.recipe_id == recipe_id:
                return task_result
        return None

    @staticmethod
    def get_start_time(recipe_id, task_id, task_results):
        """Find start_time (1st TaskResult with recipe_id:task_id) in task_results list, or use current date & time."""
        try:
            return next(filter(lambda task_result: task_result.recipe_id == recipe_id and task_result.task_id ==
                               task_id and task_result.start_time, task_results)).start_time
        except (StopIteration, AttributeError):
            return f'{datetime.utcnow()}+00:00'.replace(' ', 'T', 1)

    def output_files_from_path(self, path_prefix):
        """Return a list of files for this recipe_id:task_id in path_prefix directory."""
        path2task = os.path.join(path_prefix, f'{self.prefix_recipe_id_path}/tasks/{self.task_id}')

        all_files = []
        for dirpath, _, files in os.walk(path2task):
            all_files += [{'name': f, 'path': dirpath} for f in files]

        return all_files
