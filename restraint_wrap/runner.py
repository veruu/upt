#!/usr/bin/env python3
"""Run tests using restraint in standalone mode."""
import glob
import itertools
import os
import pathlib
import shutil

from cki.kcidb.adjust_dumpfiles import adjust_file
from cki.kcidb.utils import upload_file
from cki_lib import misc
from twisted.internet import reactor
from twisted.internet import task

from plumbing.format import ProvisionData
from plumbing.interface import ProvisionerCore
from provisioners.beaker import Beaker
from restraint_wrap.misc import attempt_reactor_stop
from restraint_wrap.restraint_protocol import RestraintClientProcessProtocol
from restraint_wrap.testplan import TestPlan
from upt.logger import LOGGER


class Runner:
    # pylint: disable=too-many-instance-attributes,no-member
    """Manage restraint protocols that process restraint output."""

    def __init__(self, provisioners, **kwargs):
        """Create the object."""
        # cmd-line arguments
        self.kwargs = kwargs
        # path to output directory
        self.output = kwargs['output']
        # number of reruns a task can have
        self.reruns = kwargs['reruns']
        # If True, output kcidb data.
        self.dump = kwargs['dump']

        # All twisted restraint client protocols
        self.protocols = []
        # Deserialize yaml, create provisioners, prepare them for use and add their protocols.
        self.provisioners = ProvisionData.prepare(provisioners, self.add_protocol,
                                                  self.on_task_result, **kwargs)
        # Build and maintain a plan of tests to run using different providers (provisioners).
        self.testplan = TestPlan(self.provisioners, **kwargs)

        # was cleanup handler completed?
        self.cleanup_done = False

        self.kernel_failed_testing = False

    def on_task_result(self, resource_group, task_result, run_suffix, fname):
        """Call when a result of a task is ready."""
        self.testplan.on_task_result(resource_group, task_result, run_suffix, fname)

    def start_protocols(self):
        """Start restraint protocols one by one."""
        for proto in self.protocols:
            if not proto.proc:
                LOGGER.debug('Starting protocol for resource_id %s...', proto.resource_group.resource_id)
                # Create & start protocol's processes, register cleanup handlers
                # and let user know.
                proto.start_all()

                # Start one by one
                break
            if proto.proc and not proto.run_suffix:
                # Don't start until directory is created
                break

    def wait_on_protocols(self):
        """Wait for hosts to finish; protocols set flags in hosts."""
        self.start_protocols()

        all_rgs = itertools.chain(*[provisioner.rgs for provisioner in self.provisioners])
        if ProvisionerCore.all_recipes_finished(all_rgs) and self.protocols:
            # If processing of all hosts is done, then stop reactor, allowing
            # the script to exit.
            attempt_reactor_stop(reactor)

    def add_protocol(self, provisioner, resource_group, adapter, **kwargs):
        """Add a restraint client wrapper (twisted protocol)."""
        failed_ssh = False
        # Ensure hosts stay up for expected time
        for host in provisioner.find_objects([resource_group], lambda obj: obj if hasattr(obj,
                                                                                          'recipe_id') else None):
            failed_ssh |= provisioner.set_reservation_duration(host)

        # restraint client protocol and process
        proto = RestraintClientProcessProtocol(provisioner, resource_group, self.add_protocol, adapter, **kwargs)

        # Add to the list of protocols.
        self.protocols.append(proto)
        if failed_ssh:
            proto.fail_kernel_testing()

        return proto

    def run(self):
        # pylint: disable=no-member
        """Run restraint client shell cmd."""
        reactor.addSystemEventTrigger('before', 'shutdown',
                                      self.cleanup_handler)

        LOGGER.debug('Runner waiting for processes to finish...')

        if self.dump:
            self.testplan.dump_testplan()

        protocols_wait_loop = task.LoopingCall(self.wait_on_protocols)
        protocols_defer = protocols_wait_loop.start(5.0)
        protocols_defer.addErrback(self.handle_runner_failure)
        protocols_defer.addCallback(self.handle_runner_failure)
        reactor.run()

        # Some provisioners may have separate console logs to download.
        rc_data = self.kwargs['rc_data']
        # If there's no rc file, the console logs will not be sanitized.
        kernel_version = rc_data.state.kernel_version if rc_data else None
        downloaded_files = self.download_logs(self.provisioners, self.output, kernel_version)

        if self.kwargs['upload'] and downloaded_files:
            name_url_output_files = self.upload_logs(self.testplan.adapter, downloaded_files)
            self.update_kcidb_dumpfiles(self.output, name_url_output_files)

        return 1 if self.kernel_failed_testing else 0

    @classmethod
    def update_kcidb_dumpfiles(cls, output, results):
        """Update all json dumpfiles with urls to system/console logfiles."""
        def adjust_func(kcidb_objects):
            for dict_obj in kcidb_objects['tests']:
                dict_obj['output_files'] = dict_obj.get('output_files', []) + name_url_output_files

            # Every file is changed.
            return True

        for ffile in glob.glob(f'{output}/results_*/*.json'):
            host_counter = str(pathlib.Path(ffile).parent.relative_to(output))
            # Add the same set of references to each file.
            name_url_output_files = [x[1] for x in results if x[0].rstrip('/') == host_counter]

            adjust_file(pathlib.Path(ffile), adjust_func)

    @classmethod
    def upload_logs(cls, adapter, host_fpath_tups):
        """Upload system logs to s3."""
        # dict objects with 'name' and 'url' keys to add to kcidb dumpfiles.
        results = []
        for host_counter, fpath in host_fpath_tups:
            upload_destination = pathlib.Path(adapter.artifacts_path, str(adapter.instance_no),
                                              fpath.relative_to(adapter.output))
            url = upload_file(adapter.visibility, upload_destination, fpath.name,
                              source_path=str(fpath))
            output_file = {'name': fpath.name, 'url': url}
            results.append((host_counter, output_file))

        return results

    @classmethod
    def download_logs(cls, provisioners, output, kernel_version):
        """Download Beaker logs."""
        downloaded_files = []
        for prov in provisioners:
            # Beaker: download console logs!
            if isinstance(prov, Beaker):
                for resource_group in prov.rgs:
                    downloaded_files += prov.download_logs(output, resource_group.recipeset.hosts,
                                                           kernel_version)
        return downloaded_files

    @classmethod
    def handle_runner_failure(cls, failure):
        """Print failure that occured in runner."""
        LOGGER.error('FAILURE in deferred: %s', failure)

    def print_overall_result(self):
        # pylint: disable=protected-access
        """Print overall result."""
        # Issues are printed to stderr.
        for proto in self.protocols:
            if proto.kernel_failed_testing:
                self.kernel_failed_testing = True
            if proto.proc:
                term_reason = proto.proc._getReason(proto.proc.status)
                if 'ended by signal 2' in str(term_reason):
                    print('*** Run aborted by user! ***')

        word = 'FAILED' if self.kernel_failed_testing else 'PASSED'
        print(f'* Kernel {word} testing!')

    def detect_abnormal_exit(self):
        # pylint: disable=protected-access
        """Detect abnormal exit in primary instance and ensure termination."""
        # It is highly suspicious when cleanup_handler runs and the hosts
        # aren't marked as "done_processing" yet.
        abnormal_exit = False
        for provisioner in self.provisioners:
            for host in provisioner.get_all_hosts(provisioner.rgs):
                # Force processing to end
                if not host._done_processing:
                    host._done_processing = True
                    abnormal_exit = True

        if abnormal_exit:
            print('* Abnormal exit!')

    @staticmethod
    def part_fname(fname):
        """Key files as (resource_id, recipe_id, task_id)."""
        # resource_id, attempt, _, recipe_id, _, task_id
        _, resource_id, _, _, recipe_id, _, task_id = fname.split('_')[:7]
        return (resource_id, recipe_id, task_id)

    @staticmethod
    def part_fdir(fdir_name):
        """Key dirs as (resource_id, recipe_id, task_id)."""
        # LOGS|RESULTS, resource_id, attempt, _, recipe_id, _, task_id
        _, resource_id, _, _, recipe_id, _, task_id = fdir_name.split('_')[:7]
        return (resource_id, recipe_id, task_id)

    @classmethod
    def move_broken_dumpfiles(cls, output):
        """Move broken re-run output from results_ to broken_results_ directories."""
        # NOTE: This works under the assumption that we always re-run tests on the same
        # resource_id, same recipe_id, same task_id. If the implementation ever changes, this will
        # need to be adjusted.
        for item in glob.glob(f'{output}/results_*'):
            for dirpath, dirs, files in os.walk(item):
                # Skip anything that isn't .json dumpfile or a dir.
                nfiles = [f for f in files if f.endswith('.json')]
                cls.move_files_or_dirs(output, nfiles, cls.part_fname, dirpath)
                ndirs = [d for d in dirs if d.startswith('LOGS_')]
                cls.move_files_or_dirs(output, ndirs, cls.part_fdir, dirpath)
                ndirs = [d for d in dirs if d.startswith('RESULTS_')]
                cls.move_files_or_dirs(output, ndirs, cls.part_fdir, dirpath)

    @classmethod
    def move_files_or_dirs(cls, output, objects, func2part, dirpath):
        """Move re-run results (logs/results/json files) away."""
        for value in misc.partition(objects, func2part).values():
            to_keep = {sorted(value)[-1]}
            to_move = set(value) - to_keep
            if not to_move:
                continue

            new_dir = pathlib.Path(f'{output}/broken_{pathlib.Path(dirpath).name}')
            new_dir.mkdir(parents=True, exist_ok=True)
            for fname in to_move:
                from_path = f'{dirpath}/{fname}'
                LOGGER.debug('Moving broken intermediate result: %s -> %s', from_path,
                             new_dir)
                try:
                    shutil.move(from_path, new_dir)
                except FileNotFoundError:
                    LOGGER.error('Failed moving %s -> %s', from_path, new_dir)

    def cleanup_handler(self):
        """Do cleanup and evaluate retcodes from hosts."""
        if self.cleanup_done:
            return

        LOGGER.debug('* Runner cleanup handler runs...')

        # Print overall result.
        self.print_overall_result()

        # Move broken re-run results away.
        self.move_broken_dumpfiles(self.output)

        # Detect abnormal exit and print if it occurred.
        self.detect_abnormal_exit()

        self.cleanup_done = True

    def do_main(self):
        """Do all."""
        os.makedirs(self.output, exist_ok=True)

        return self.run()
