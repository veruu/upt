#!/usr/bin/env python3
"""Main entry point, command-line interface for upt."""
import itertools
import os
import pathlib
import sys
import time

from cki_lib.logger import logger_add_fhandler
import click
from rcdefinition.rc_data import SKTData

from plumbing.format import ProvisionData
from restraint_wrap.misc import RESTRET
from restraint_wrap.misc import add_directory_suffix
from restraint_wrap.runner import Runner
from upt.logger import LOGGER
from upt.misc import RET

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


def wait_on_done(input_fname):
    """Wait until all provisioners within a file are done."""
    print('restraint test runner waiting on provisioners...')

    not_done = True
    instance_no = 1
    provisioners = None
    while not_done:
        # Don't touch provisioner data, just read the status attribute.
        prov_data = ProvisionData.deserialize_file(input_fname)
        provisioners = prov_data.provisioners
        instance_no = prov_data.instance_no
        rgs = list(itertools.chain(*[provisioner.rgs for provisioner in provisioners]))
        if not rgs:
            LOGGER.debug("Input file doesn't have any resources to test on.")
            sys.exit(RESTRET.INFRASTRUCTURE_ISSUE.value)

        not_done = any(rg.status == str(RET.PROVISIONING_WAITING) for rg in rgs)
        if not_done:
            time.sleep(60)

    return instance_no, provisioners


@click.group(context_settings=CONTEXT_SETTINGS)
@click.pass_context
def cli(ctx):
    """Run test using restraint's standalone mode."""
    ctx.ensure_object(dict)


@cli.command()
@click.option('-i', '--input', required=True, default='c_req.yaml',
              help='Path to input file (provisioning request). '
                   'Default: c_req.yaml.')
@click.option('-o', '--output', required=True, default='run',
              help='Path to output directory. For "run", a "run.done.01" directory will be'
                   ' created. Please check that the directory does not already exist.')
@click.option('--keycheck/--no-keycheck', default=False,
              help='Do strict ssh host keychecking. Default: False.')
@click.option('--reruns', required=True, default=1, type=int,
              help='How many times to re-run tasks that are to be rerun based on '
                   'rules (e.g. EWD hits).')
@click.option('--rc', default=os.environ.get('RC_FILE', 'rc'), help='Path to rc file. Defaults to RC_FILE env. '
                                                                    'variable or rc.')
@click.option('--dump/--no-dump', required=True, default=False,
              help='If True, dump kcidb files. Default: False.')
@click.option('--upload/--no-upload', required=True, default=False,
              help='If True, upload files to s3 buckets. If False, do not. Default: False.')
def test(**kwargs):
    """Run tests using restraint."""
    # Open rc-file, deserialize it
    path2rc = pathlib.Path(kwargs['rc'])
    kwargs['rc_data'] = SKTData.deserialize(path2rc.read_text()) if path2rc.is_file() else None
    if not kwargs['rc_data'] and (kwargs['dump'] or kwargs['upload']):
        raise RuntimeError('please specify valid rc file using --rc to generate pipeline '
                           'dumpfiles and upload artifacts')

    # Ensure we're ready to roll (do kernel testing) and save instance number for --async.
    kwargs['instance_no'], provisioners = wait_on_done(kwargs['input'])
    output = kwargs['output'] = add_directory_suffix(kwargs['output'], kwargs['instance_no'])
    logger_add_fhandler(LOGGER, 'info.log', output)

    kwargs['keycheck'] = 'yes' if kwargs['keycheck'] else 'no'
    runner = Runner(provisioners, **kwargs)

    retcode = runner.do_main()
    LOGGER.debug('runner retcode on sys.exit: %d', retcode)
    sys.exit(retcode)


def main():
    # pylint: disable=no-value-for-parameter
    """Do all."""
    cli()


if __name__ == '__main__':
    main()
