"""Misc utility methods."""
from enum import Enum
import os
from urllib.parse import quote

from twisted.internet import error


class RESTRET(Enum):
    """Wraps all restraint runner return codes."""

    # All good, kernel is fine.
    KERNEL_TESTING_PASSED = 0

    # Single or multiple infrastructure issues were hit and were not recovered by rerunning.
    # Getting too many of these means our ability to test the kernel was limited and we may have
    # to fail the pipeline.
    INFRASTRUCTURE_ISSUE = 1

    # A serious issue was found (e.g. a test failed, kernel panicked, ...).
    KERNEL_TESTING_FAILED = 2

    @classmethod
    def merge_retcode_values(cls, retcode_values):
        """Combine multiple restraint runner retcode values like 0, 1, 2 into one."""
        retcodes = []
        for retcode in retcode_values:
            for item in RESTRET:
                if item.value == retcode:
                    retcodes.append(item)

        return cls.merge_retcodes(retcodes)

    @classmethod
    def merge_retcodes(cls, retcodes):
        """Combine multiple restraint runner retcodes into one.

        Only the most severe retcode from the list will be returned.
        """
        for retcode in retcodes:
            assert retcode in RESTRET

        return max(retcodes, key=lambda x: x.value, default=cls.KERNEL_TESTING_FAILED)


def add_directory_suffix(output, instance_no):
    """Add numerical .done.XY suffix to directory."""
    dirname = f'{os.path.abspath(output)}.done.{instance_no:02d}'
    assert not os.path.isdir(dirname)
    return dirname


def attempt_reactor_stop(reactor):
    """If twisted reactor is running, try to stop it."""
    if reactor.running:
        try:
            reactor.callFromThread(reactor.stop)
        except error.ReactorNotRunning:
            pass


def attempt_heartbeat_stop(heartbeat_loop):
    """Attempt to stop heartbeat looping call."""
    try:
        heartbeat_loop.stop()
    except (AssertionError,  AttributeError):
        pass


def convert_path_to_link(path, is_file):
    """Get a quoted url to path, or just path if we're not running in pipeline."""
    prefix = os.environ.get('CI_JOB_URL', '')
    prefix += f'/artifacts/{"file" if is_file else "browse"}/artifacts/' if prefix else ''

    return prefix + quote(str(path)) if prefix else path
